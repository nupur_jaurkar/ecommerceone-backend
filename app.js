'use strict';

var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();
var db = require('./db')
var express = require('express')
// const socketio = require('socket.io');
// const socketEvents = require('./web/socket'); 


module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};
app.use(express.static('public'));


SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }
  app.use(swaggerExpress.runner.swaggerTools.swaggerUi());

  // install middleware
  swaggerExpress.register(app);

  var port = process.env.PORT || 10010;
  app.listen(port);

  if (swaggerExpress.runner.swagger.paths['/hello']) {
    console.log('try this:\ncurl http://127.0.0.1:' + port + '/hello?name=Scott');
  }
});
