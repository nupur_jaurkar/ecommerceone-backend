'use strict';
var util = require('util'),
    ProjectModel = require('../../schemas/users'),
    jwt = require('jsonwebtoken'),
    bcrypt = require('bcrypt');
;

module.exports = {
    registration: registration,
    login: login,
    logout: logout,
};

//Registration api
let BCRYPT_SALT_ROUNDS = 12;
function registration(req, res) {
    console.log(" req.body  =============", req.body);
    bcrypt.hash(req.body.password, BCRYPT_SALT_ROUNDS)
        .then(function (hashedPassword) {
            console.log('req.body.password', req.body.passsword)
            console.log('hasedpaswd', hashedPassword)
            // Store hash in your password DB.
            var record = new ProjectModel();
            record.username = req.body.username,
                record.password = hashedPassword,
                record.email = req.body.email,
                record.role = "user"
            console.log(record);
            record.save(function (err, data) {
                console.log('in signup')
                if(err){
                    res.send({ code: 11000})
                }
                else{
                    res.send({ code: 200, data: data });
                }
            });
        });

    // var username = req.body;
    // var password = req.body.password;
    // var email = req.body.email
}




//Login api
function login(req, res) {
    console.log("inside login in swagger homepage.js", req.body);
    // let data = {
    //     username: req.body.username,
    //     passsword: req.body.passsword,          
    // };
    // console.log('data',data)
    try {
        ProjectModel.findOne({
            username: req.body.username,
            // password: req.body.password
        }, (err, user) => {
            if (err) {
                return res.json({ err: err, code: 404, message: "Something went wrong!" });
            }
            else if (user) {
                bcrypt.compare(req.body.password, user.password, function (err, passwordMatched) {
                    console.log('passwordMatched ::', passwordMatched);
                    if (passwordMatched) {
                        const token = jwt.sign(user.toJSON(), 'nupur', {
                            expiresIn: 1404 //expires in 1 hour
                        });
                        // console.log("token ::", token);
                        return res.json({
                            code: 200,
                            message: "success",
                            data: user,
                            token: token
                        });
                    }
                    else {
                        res.json({
                            code: 403,
                            message: 'Username or password invalid'
                        });
                    }
                    // res == true
                });
                // user.token=token;

                // return res.json({data:user,token:token, code:200,'message':'SUccess'});
            }
            else {
                return res.json({
                    code: 404,
                    message: "User doesn't exist"
                })
            }
        })
    } catch (ex) {
        console.log('Exception :: ', ex);
    }
}






//logout api
function logout(req, res) {
    req.session.destroy(function (err) {
        if (err) {
            console.log(err);
            Response.errorResponse(err.message, res);
        }
        else {
            Response.successResponse('You logged out successfully!', res, {});
        }
    });
}