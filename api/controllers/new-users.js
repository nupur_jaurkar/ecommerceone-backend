var ProjectModel = require('../../schemas/users'),
    util = require('util');

module.exports = {
    addUsers:addUsers,
    listingUsers:listingUsers,
    editUsers:editUsers,
    deleteUsers:deleteUsers,
};

//add new users
function addUsers(req,res){
    console.log(" req.body  =============",req.body.role);
    var record = new ProjectModel();
        record.username = req.body.username,
        record.password = req.body.password,
        record.email = req.body.email,
        record.role = req.body.role,
        console.log(record);
        record.save(function (err, data) {
            console.log('in add users')
            // res.send({code:200, data:data});   
            res.send({code:200,message:'User added successfully.', data:data});     
        });
}

//list users
function listingUsers(req,res){
    console.log("in listingUsers");
    ProjectModel.find(function (err, data) {
        res.send(data);
    })
}

//edit users
function editUsers(req,res){
    console.log("in editUsers",res)
    var _id = req.swagger.params.id.value

    ProjectModel.findByIdAndUpdate(_id, req.body, function (err, data) {
         if (err) {throw err;}
            else {res.json(data);} 
        // if (err) return next(err);
        // res.json(data);
      });
}

//delete users
function deleteUsers(req,res){
    var id = req.swagger.params.id.value
    console.log("in delete and id: ", id);
    ProjectModel.findByIdAndRemove({_id:id}, function (err, data) {
            if (err) {throw err;}
            else {res.json(data);}           
          });
}