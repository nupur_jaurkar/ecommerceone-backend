var util = require('util'),
    fs = require('fs'),
    ProductModel = require('../../schemas/product')
;
module.exports={
    addProducts:addProducts,                    //operation id: Function name
    listingProducts:listingProducts,
    deleteProducts:deleteProducts,
    editProducts:editProducts
};

//add products 
function addProducts(req,res){
    console.log(" req.body  =============",req.body);

    var timestamp = Date.now();
    var originalImageName = timestamp + "_" + req.files.file[0].originalname;
    var imagePath = "../swagger_backend/public/images/"+ originalImageName;
    fs.writeFile(imagePath,(req.files.file[0].buffer),function(err){
        if(err) {throw err}
        console.log("Product Image uploaded!")
    })
    var record = new ProductModel();
        record.productName=req.swagger.params.productName.value;
        record.productDescription=req.swagger.params.productDescription.value;
        record.productCost=req.swagger.params.productCost.value;
        record.productCategory=req.swagger.params.productCategory.value;
        record.file="http://localhost:10010/images/"+ originalImageName;
        record.save(function (err, data) {
            console.log('in addProduct')
            res.send({code:200,message:'Product added successfully.', data:data});    
          });
}

//Listing products
function listingProducts(req,res){
    console.log("in listingProducts");
    ProductModel.find(function (err, data) {
        res.send(data);
    })
}

//Delete products
function deleteProducts(req,res){
    var id = req.swagger.params.id.value
    console.log("in delete and id: ", id);
    ProductModel.findByIdAndRemove({_id:id}, function (err, data) {
            if (err) {throw err;}
            else {res.json(data);}           
          });
}

//edit products
function editProducts(req,res){
    console.log("in editProducts",req)
    var _id = req.swagger.params.id.value
    var timestamp = Date.now();
    var originalImageName = timestamp + "_" + req.files.file[0].originalname;
    // console.log("thi=s ==============",originalImageName);
    var imagePath = "../swagger_backend/public/images/"+ originalImageName;
    fs.writeFile(imagePath,(req.files.file[0].buffer),function(err){
        if(err) {throw err}
        console.log("Image updated!")
    })    
    var productName = req.body.productName;
    var productDescription = req.body.productDescription;
    var productCost = req.body.productCost;
    var productCategory = req.body.productCategory;
    var file = "http://localhost:10010/images/"+ originalImageName;

    ProductModel.findByIdAndUpdate(_id,{ $set: 
        { productName:productName, 
        productDescription:productDescription,
        productCost:productCost,
        productCategory:productCategory,
        file:file }},        
        function (err, data) {
         if (err) {throw err;}
            else {console.log("data:", data)} 
            data.save();
            res.json({data:data,code:200})
    });
}

//../frontend/src/assets/images/ProductUploads/