var ProjectModelCart = require('../../schemas/userCart'),
ProjectModelShipping = require('../../schemas/shipping_details')
// Set your secret key: remember to change this to your live secret key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
stripe = require("stripe")("sk_test_bN0IKzL3eeH7lqQ9CB5ivqXu");


// let payableAmount=0;

module.exports = {
    addToCart: addToCart,                        //operation id: Function name
    listCartProducts:listCartProducts,
    deleteCartProduct:deleteCartProduct,
    stripePayWithoutToken:stripePayWithoutToken,
    saveShippingDetails:saveShippingDetails,
    payment:payment,
    payment_success:payment_success,
};


//==================add products to cart
// function addToCart(req,res){
//     var cartRecord = new ProjectModelCart();
//     cartRecord.quantity = 1;  //left side variables are property names inside schema declaration and right side variables are 'FormControlName' attribute values in index.html
//     cartRecord.productName = req.body.productName;
//     cartRecord.productId = req.body.productId;
//     cartRecord.productCost = req.body.productCost;
//     cartRecord.userId = req.body.userId;

//     // cartRecord.totalCost = req.body.totalCost;
//     // cartRecord.is_delete = false;
//     console.log(cartRecord.userId);
//     var id = cartRecord.userId;

//     ProjectModelCart.findOne({ 'productId': cartRecord.productId }, function (err, data) {
//         if (err) throw err;
//         else if (data) {
//             console.log("same product");
//             console.log("Complete Product Data",data);
//             data.quantity = Number(data.quantity + 1);
//             data.totalCost = (cartRecord.productCost * data.quantity);
//             console.log('total cost==========',data.totalCost)
//             ProjectModelCart.findOneAndUpdate({ "productid": data.productId }, { $set: data }, function (err, result) {
//                 if (err) throw err;
//                 else{
//                     res.json(result);
//                 }
//                 console.log("Cart updated");
//               });        
//         }
//         else {
//             cartRecord.save(function (err, response) {
//                 if (err) {
//                     res.json({
//                         code: 404,
//                         message: 'CartDetails not Added'
//                     })
//                 } else {
//                     res.json({
//                         code: 200,
//                         data: response
//                     })
//                     console.log('Successfully added to cart');

//                 }

//             })

//         }
//     })

// }

//========add product to cart
function addToCart(req, res) {
    debugger;
    var cartRecord = new ProjectModelCart();
    console.log("request", req.body);
    cartRecord.quantity = 1;
    cartRecord.userId = req.body.userId;
    cartRecord.productId = req.body.productId;
    cartRecord.productName = req.body.productName;
    cartRecord.productCost = req.body.productCost;

    ProjectModelCart.findOne({ productId: req.body.productId }, function (err, result) {
        if (err) {
            console.log('error findone--------------,',err);
        } else if (result) {
            console.log(result);
            var quant = result.quantity + 1
            var totalPrice=0;
            totalPrice = parseInt(quant) * cartRecord.productCost;
            console.log(quant, " ===nupur ===", totalPrice)
            result.quantity = quant
            console.log("result.quantity",result.quantity)
            
            ProjectModelCart.findOneAndUpdate({ productId: req.body.productId },
                {
                    $set: {
                        productCost: totalPrice,
                        quantity: quant
                    }
                }, function (err, data) {

                    if (err) {
                        console.log(err);

                    } else {
                        res.json({
                            code: 200,
                            data: data
                        })

                    }
                })


        } else {
            cartRecord.save(function (err, response) {
                if (err) {
                    res.json({
                        code: 404,
                        message: "product not added"
                    })
                 } else {
                    res.json({
                        code: 200,
                        data: response
                    })
                }
            })
        }
    }
    )
}


//==========list content in cart
function listCartProducts(req, res) {
    console.log(req.swagger.params.id.value);

    ProjectModelCart.find({ userId: req.swagger.params.id.value })
    .sort({ date: 'descending' })
    .populate({
        
        path: 'productId',
        model: 'products',          //name of collection 

    }).exec(function (err, data) {
        console.log(err)
        // var data={value:'payableAmount'}
        res.json(data);

        let payableAmount = 0;
        for(let i=0; i<data.length;i++){
            // console.log(payableAmount +'  and  '+ data[i].productCost );
            payableAmount = payableAmount + data[i].productCost 
        }
            console.log('payableamount====',payableAmount)
    })
}

//         if(code=200)
//     {
//         let payableAmount = 0;
//         for(let i=0; i<data.length;i++){
//             console.log(data.totalCost +'  and  '+ data[i].totalCost );
//             payableAmount = data.totalCost + data[i].totalCost 
//         }

// //         data.forEach(function (element) {
// // console.log('elemt---------',element);
// //         })
// //         console.log('data.length',data.length)
//         console.log('paybleamountbackend====',payableAmount)
//     }
//     else(console.log(err))
    // })
    
    // var payableAmount = 0;
    // for (var i = 0; i < totalCost.length; i++) {
    //   if (input[i].checked) {
    //     total += parseFloat(input[i].value);
    //   }
    // }
// }


//delete products from cart
function deleteCartProduct(req,res){
    var id = req.swagger.params.id.value
    console.log("in delete and id: ", id);
    ProjectModelCart.findByIdAndRemove({_id:id}, function (err, data) {
            if (err) {throw err;}
            else {res.json(data);}           
          });
}


//saving shipping-form data
function saveShippingDetails(req,res){
    var record = new ProjectModelShipping();
        record.name = req.body.name,
        record.address = req.body.address,
        record.pincode = req.body.pincode,
        record.contact = req.body.contact,
        record.email = req.body.email,
        record.userId = req.body.userId;
        console.log(record);
        record.save(function (err, data)
         {
            console.log('in saveshippingDet')
            res.send({code:200, data:data});    
        });
}


//===========stripe without saving card
function stripePayWithoutToken(card_number, exp_month, exp_year, cvc, amount, description, currency) {
    return new Promise(function (resolve, reject) {
        generateCardToken(card_number, exp_month, exp_year, cvc)
            .then((tokenResponse) => {
                if (tokenResponse && tokenResponse.id) {
                    stripePayUsingToken(tokenResponse.id, amount, description, currency)
                        .then((paymentResponse) => {
                            if (paymentResponse) {
                                return resolve(paymentResponse);
                            } else {
                                return reject(constant.SOMETHING_WENT_WRONG);
                            }
                        }).catch(err => {
                            return reject(err);
                        })
                } else {
                    return reject(constant.SOMETHING_WENT_WRONG);
                }
            }).catch(err => {
                return reject(err);
            })
    })
}

//payment api stripe
function payment(req,res){

// Set your secret key: remember to change this to your live secret key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
var stripe = require("stripe")("sk_test_bN0IKzL3eeH7lqQ9CB5ivqXu");
// Token is created using Checkout or Elements!
// Get the payment token ID submitted by the form:
const token = req.body.token;      // Using Express
const payableAmount= req.body.amount;
const priceToBePaid=payableAmount*100 
console.log('token========',token,'payableamount in payment()===',payableAmount)

// const amnt = req.body.amnt
// console.log('amnt=====',amnt)
const charge = stripe.charges.create({
    amount: priceToBePaid,
    currency: 'inr',
    description: 'Example charge',
    source: token,
  });
 var data={
    msg:'success',
    charge:charge
 };
 res.json(data);

//  if(code==200)
//  {
//     //  amount = 0;
//     localStorage.setItem(payableAmount,0)
//  }

// // Token is created using Checkout or Elements!
// // Get the payment token ID submitted by the form:
// const token = request.body.stripeToken; // Using Express

// const charge = stripe.charges.create({
//   amount: 999,
//   currency: 'usd',
//   description: 'Example charge',
//   source: token,
// });
}

//payment_success api with delete cart item
function payment_success(req,res){
    // let id = req.swagger.params.id.value
    let userid =req.swagger.params.id.value
    console.log('payment_succes userid:===',userid)
    ProjectModelCart.remove({userId:userid},(err,data)=>{
        if(err){ throw err }
        else{ res.json(data) }
    })
} 
    
 

