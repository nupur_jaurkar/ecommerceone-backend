var mongoose = require('mongoose')

module.exports = mongoose.model('shippingDetails',{
    name: {type: String},
    address: {type: String},
    pincode: {type: Number},
    contact: {type: Number},
    email: {type:String},
    userId: {
        type:mongoose.Schema.Types.ObjectId,
        ref:'users'
    },
})