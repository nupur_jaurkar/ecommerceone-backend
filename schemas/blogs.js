var mongoose = require('mongoose');

module.exports = mongoose.model('blogs', {
    name: {type :String},
    description:{type :String},
    file:{type: String}
});
