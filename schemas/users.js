var mongoose = require('mongoose');

module.exports = mongoose.model('users', {
    username: {type :String},
    password:{type :String},
    email : {type :String, unique : true},
    role: {type :String, default : 'user'}
});
