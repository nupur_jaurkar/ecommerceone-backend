var mongoose = require('mongoose');

module.exports = mongoose.model('userCart', {
    quantity:{type: Number}, 
    productName: {type :String},
    productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'products'
    },
    productCost:{type :Number},
    // totalCost:{type:Number, default: 0},
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users'
    },
    is_deleted: false,
});
