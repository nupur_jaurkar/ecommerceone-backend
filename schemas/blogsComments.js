var mongoose = require('mongoose');

module.exports = mongoose.model('blogsComments', {
    comments: {type :String},
    blogId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'blogs'
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users'
    },
    rating:{type: Number},
    date:{type: Date, default: Date.now()}
});
